@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>{{ $post->title }}</h3>
            </div>
            <div class="panel-body">
                {{ $post->body }}
            </div>
        </div>
        {!!
            link_to_route(
                'posts.index',
                'Go Back',
                [],
                ['class' => 'btn btn-warning btn-block']
            )
        !!}
    </div>
@endsection