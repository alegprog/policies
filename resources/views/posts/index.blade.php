@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @can('create', \App\Post::class)
                {{
                    link_to_action(
                        'PostController@create',
                        'Add a new Post',
                        [],
                        ["class" => "btn btn-md btn-success pull-right"]
                    )
                }}
            @endcan

            <div class="clearfix"></div>

            <hr />

            @forelse($posts as $post)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>{{ $post->title }}</h3>
                    </div>
                    <div class="panel-body">
                        {{ $post->body }}
                    </div>
                    <div class="panel-footer" style="height: 45px;">
                        @can('update', $post)
                            {{
                                link_to_action(
                                    'PostController@edit', 'Edit', ['id' => $post->id], ['class' => 'btn btn-xs btn-primary col-md-2']
                                )
                            }}
                        @endcan

                        @can('view', $post)
                            {{
                                link_to_action(
                                    'PostController@show', 'Detail', ['id' => $post->id], ['class' => 'btn btn-xs btn-info col-md-2']
                                )
                            }}
                        @endcan

                        @if($post->trashed())
                            @can('restore', $post)
                                {!! Form::open(['method' => 'PATCH', 'route' => ['posts.restore', $post->id]]) !!}
                                {!! Form::submit('Restore', ["class" => "btn btn-xs btn-warning col-md-2"]) !!}
                                {!! Form::close() !!}
                            @endcan
                        @else
                            <span class="pull-right">
                                @can('delete', $post)
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id]]) !!}
                                    {!! Form::submit('Destroy', ["class" => "btn btn-xs btn-danger"]) !!}
                                    {!! Form::close() !!}
                                @endcan
                            </span>
                        @endif

                    </div>
                </div>
            @empty
                <div class="alert alert-danger">
                    Posts not found!
                </div>
            @endforelse
        </div>
    </div>
@endsection