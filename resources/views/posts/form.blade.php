@extends('layouts.app')


@section('content')

    <div class="container">

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(isset($post))
            {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT', 'name' => 'formPosts']) !!}
        @else
            {!! Form::open(['action' => 'PostController@store']) !!}
        @endif

        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('body', 'Body') !!}
            {!! Form::textarea('body', old('body'), ['class' => 'form-control']) !!}
        </div>

        @if(isset($post))
            {!! Form::submit('Update a Post', ['class' => 'btn btn-success btn-block']) !!}
        @else
            {!! Form::submit('Add a new Post', ['class' => 'btn btn-success btn-block']) !!}
        @endif

        {!! link_to_route('posts.index', 'Go Back', [], ['class' => 'btn btn-warning btn-block']) !!}

        {!! Form::close() !!}

    </div>

@endsection