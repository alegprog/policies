<?php

namespace App\Http\Requests;

use App\Post;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('validate', Post::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'title' => 'required|unique:posts|min:5|max:190',
                    'body' => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'title' => 'required|min:5|max:190',
                    'body' => 'required'
                ];
            default:break;
        }
    }
}
