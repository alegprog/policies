<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);
        return view('posts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostRequest $postRequest
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $postRequest)
    {
        Post::create($postRequest->all());
        return back()->with('message', ['success', 'El post se ha creado correctamente']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $this->authorize('view', $post);
        return view('posts.detail', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.form', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostRequest  $postRequest
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $postRequest, $id)
    {
        $post = Post::findOrFail($id);
        $this->authorize('update', $post);
        $post->fill($postRequest->all())->save();
        return back()->with('message', ['success', 'El post se ha actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $this->authorize('delete', $post);
        $post->delete();
        //$post->forceDelete();
        return back()->with('message', ['success', 'El post se ha eliminado correctamente']);
    }

    public function restore($id)
    {
        $post = Post::findOrFail($id);
        $post->restore();
        return back()->with('message', ['success', 'El post se ha restaurado correctamente']);
    }
}

